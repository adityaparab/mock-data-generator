import { injectable } from 'inversify';
import { IMockDataGenerator } from './models/IMockDataGenerator';

@injectable()
export class MockDataGenerator implements IMockDataGenerator {
    public run() {
        console.log('MockDataGenerator :: Run');
    }
}
