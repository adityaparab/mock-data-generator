import { Container } from 'inversify';
import { MockDataGenerator } from './MockDataGenerator';
import { IMockDataGenerator, TYPES } from './models';

export const MockDataGeneratorContainer: Container = new Container();

MockDataGeneratorContainer.bind<IMockDataGenerator>(TYPES.MockDataGenerator).to(
    MockDataGenerator
);
