// tslint:disable: interface-name max-classes-per-file

export class InvalidArrayLengthError extends Error { }

export class InvalidArrayContentError extends Error { }
