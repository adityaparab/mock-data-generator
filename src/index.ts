import 'reflect-metadata';
import { MockDataGeneratorContainer } from './container';
import { IMockDataGenerator, TYPES } from './models';
import './utils/prototypes';

const mockDataGenerator = MockDataGeneratorContainer.get<IMockDataGenerator>(TYPES.MockDataGenerator);

mockDataGenerator.run();
