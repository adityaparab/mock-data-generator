export interface IAsset {
    category: string;
    description: string;
    value: string;
}
