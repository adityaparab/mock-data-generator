export interface IBeneficiary {
    beneficiary: string;
    percent: number;
    type: string;
}
