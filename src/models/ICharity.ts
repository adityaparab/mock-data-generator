import { IAsset } from './IAsset';

export interface ICharity {
    name: string;
    assets: IAsset[];
    liabilities: IAsset[];
}
