import { ICharity } from './ICharity';
import { IHeir } from './IHeir';
import { ITrust } from './ITrust';

export interface IDistribution {
    charities: ICharity[];
    heirs: IHeir[];
    trusts: ITrust[];
}
