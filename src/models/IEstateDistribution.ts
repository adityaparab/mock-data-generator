import { IFirstDeath } from './IFirstDeath';
import { ILastDeath } from './ILastDeath';
import { ILink } from './ILink';
import { INetWorth } from './INetWorth';

export interface IEstateDistribution {
    firstDeath: IFirstDeath;
    lastDeath: ILastDeath;
    links: ILink[];
    netWorth: INetWorth;
}
