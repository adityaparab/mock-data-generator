import { ISharedAsset } from './ISharedAsset';
import { ITaxesAndExpenses } from './ITax';
import { IDistribution } from './IDistribution';
import { ISurvivor } from './ISurvivor';

export interface IFirstDeath {
    inEstateAtFirstDeath: ISharedAsset;
    inEstateAfterFirstDeath: ISharedAsset;
    distributions: ISurvivor;
    outOfEstate: IDistribution;
    taxesAndExpenses: ITaxesAndExpenses;
}
