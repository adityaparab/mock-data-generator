import { IAsset } from './IAsset';

export interface IHeir {
    name: string;
    type: string;
    assets: IAsset[];
    liabilities: IAsset[];
}
