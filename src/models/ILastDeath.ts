import { ISharedEstate } from './ISharedEstate';
import { IDistribution } from './IDistribution';

export interface ILastDeath {
    distributions: IDistribution;
    inEstate: ISharedEstate;
    outOfEstate: IDistribution;
}
