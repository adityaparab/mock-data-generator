export interface ILink {
    rel: string;
    method: string;
    href: string;
}
