export interface IMockDataGenerator {
    run(): void;
}
