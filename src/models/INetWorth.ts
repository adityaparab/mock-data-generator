import { ISharedEstate } from './ISharedEstate';
import { IDistribution } from './IDistribution';

export interface INetWorth {
    inEstate: ISharedEstate;
    outOfEstate: IDistribution;
}
