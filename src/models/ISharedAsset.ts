export interface ISharedAsset {
    category: string;
    description: string;
    clientValue: number;
    jointValue: number;
    spouseValue: number;
}
