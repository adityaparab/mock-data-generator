import { ISharedAsset } from './ISharedAsset';

export interface ISharedEstate {
    assets: ISharedAsset[];
    liabilities: ISharedAsset[];
}