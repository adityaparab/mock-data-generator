import { ICharity } from './ICharity';
import { IHeir } from './IHeir';
import { ITrust } from './ITrust';
import { ISharedEstate } from './ISharedEstate';

export interface ISurvivor {
    charities: ICharity[];
    heirs: IHeir[];
    trusts: ITrust[];
    survivor: ISharedEstate;
}