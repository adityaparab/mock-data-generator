export interface ITaxesAndExpenses {
    estateTax: number;
    incomeTaxWithRespectToDecedent: number;
    probateAndExpenses: number;
    stateDeathTax: number;
}
