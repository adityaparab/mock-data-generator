import { IAsset } from './IAsset';
import { IBeneficiary } from './IBeneficiary';

export interface ITrust {
    type: string;
    isSprinkleTrust: boolean;
    termLifetime: string;
    outOfEstatePercent: number;
    distribution: string;
    trustee: string;
    gsttExempt: string;
    establishedDate: string;
    name: string;
    incomeBeneficiaries: IBeneficiary[];
    remainderBeneficiaries: IBeneficiary[];
    assets: IAsset[];
    liabilities: IAsset[];
}
