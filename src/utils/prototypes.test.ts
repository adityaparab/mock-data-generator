import './prototypes';

describe('Test Array.prototype.random method', () => {

    it('should return a random number from the array', () => {
        const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        const random = arr.random();
        const isNumberPresentInArray = arr.includes(random);

        expect(isNumberPresentInArray).toBe(true);
    });
});

describe('Test Array.prototype.randomInRange method', () => {
    it('should test if random number between first and last element of an array is returned', () => {
        const arr = [1, 100];
        const random = arr.randomInRange();
        const isNumberValid = random >= arr[0] && random <= arr[1];

        expect(isNumberValid).toBe(true);
    });
});
