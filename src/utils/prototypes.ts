// tslint:disable: interface-name
interface Array<T> {
    random(): T;
    randomInRange(): T;
}

Array.prototype.random = function random() {
    return this[Math.floor(Math.random() * this.length)];
};

Array.prototype.randomInRange = function randomInRange() {
    return Math.floor(Math.random() * (this[1] - this[0] + 1)) + this[0];
};
